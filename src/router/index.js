import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
// home
const Home = r => require.ensure([], () => r(require('@/pages/home')), 'home')

// 手表展示
const ShowWatch = r => require.ensure([], () => r(require('@/pages/showWatch')), 'showWatch')

// 验证序列号
const Serialnumber = r => require.ensure([], () => r(require('@/pages/serialnumber')), 'serialnumber')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/helloWorld',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/showWatch',
      name: 'ShowWatch',
      component: ShowWatch
    },
    {
      path: '/serialnumber',
      name: 'Serialnumber',
      component: Serialnumber
    }
  ]
})
